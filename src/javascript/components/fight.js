import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let playerOne = firstFighter;
    let playerTwo = secondFighter;

    const firstHealthBar = document.getElementById('left-fighter-indicator');
    const secondHealthBar = document.getElementById('right-fighter-indicator');
    const healthOne = playerOne.health;
    const healthTwo = playerTwo.health;
    let isCriticalOneDisabled = false;
    let isCriticalTwoDisabled = false;

    const fighterOneAttack = () => {
      const health = getDamage(playerOne, playerTwo);
      if (health <= 0) {
        resolve(playerOne);
      } else {
        //playerTwo.health = health;
        playerTwo.health = health;
        const resultOne = (health * 100) / healthTwo;
        secondHealthBar.style.width = `${resultOne}%`;
      }
    };

    const fighterTwoAttack = () => {
      const health = getDamage(playerTwo, playerOne);
      if (health <= 0) {
        resolve(playerTwo);
      } else {
        playerOne.health = health;
        const resultTwo = (health * 100) / healthOne;
        firstHealthBar.style.width = `${resultTwo}%`;
      }
    };

    const fighterOneAttackFighterTwoBlock = () => {};

    const fighterTwoAttackFighterOneBlock = () => {};

    const fighterOneCrit = () => {
      if (!isCriticalOneDisabled) {
        const health = criticalAttack(playerOne, playerTwo);
        isCriticalOneDisabled = true;
        setTimeout(() => (isCriticalOneDisabled = false), 10000);
        if (health <= 0) {
          secondHealthBar.style.width = '0%';
          resolve(playerOne);
        } else {
          playerTwo.health = health;
          const resultOne = (health * 100) / healthTwo;
          secondHealthBar.style.width = `${resultOne}%`;
        }
      }
    };

    const fighterTwoCrit = () => {
      if (!isCriticalTwoDisabled) {
        const health = criticalAttack(playerTwo, playerOne);
        isCriticalTwoDisabled = true;
        setTimeout(() => (isCriticalTwoDisabled = false), 10000);
        if (health <= 0) {
          resolve(playerTwo);
          firstHealthBar.style.width = '0%';
        } else {
          playerOne.health = health;
          const resultTwo = (health * 100) / healthOne;
          firstHealthBar.style.width = `${resultTwo}%`;
        }
      }
    };
    let pressed = new Set();
    function runOnKeys(func, codes) {
      window.addEventListener('keydown', (event) => {
        pressed.add(event.code);

        for (let code of codes) {
          if (!pressed.has(code)) {
            return;
          }
        }
        pressed.clear();
        func();
      });

      document.addEventListener('keyup', function (event) {
        pressed.delete(event.code);
      });
    }

    runOnKeys(fighterOneAttackFighterTwoBlock, [controls.PlayerOneAttack, controls.PlayerTwoBlock]);
    runOnKeys(fighterTwoAttackFighterOneBlock, [controls.PlayerTwoAttack, controls.PlayerOneBlock]);
    runOnKeys(fighterOneAttack, [controls.PlayerOneAttack]);
    runOnKeys(fighterTwoAttack, [controls.PlayerTwoAttack]);
    runOnKeys(fighterOneCrit, controls.PlayerOneCriticalHitCombination);
    runOnKeys(fighterTwoCrit, controls.PlayerTwoCriticalHitCombination);
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  if (attacker.attack <= defender.defense) {
    return defender.health;
  } else {
    return defender.health - (getHitPower(attacker) - getBlockPower(defender));
  }
}

export function getHitPower(fighter) {
  // return hit power
  return fighter.attack * criticalHitChance();
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() * (2 - 1) + 1;
  // return block power
  return fighter.defense * dodgeChance;
}
export function criticalHitChance() {
  return Math.random() * (2 - 1) + 1;
}

export function criticalAttack(attacker, defender) {
  return (defender.health = 2 * attacker.attack);
}
