import { createElement } from '../helpers/domHelper';
import {createHealthIndicator} from './arena';


export function createFighterPreview(fighter, position) {
  if(!fighter) {
    return null;
  }
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)
  const imgElement = createFighterImage(fighter);
  const indicator = createHealthIndicator(fighter, position);
  const stats = createElement({
    tagName: 'div',
    className: `left-fighter`,
  });
  stats.innerText = `Atack: ${fighter.attack}, Defense: ${fighter.defense}`;
  fighterElement.append(indicator, imgElement, stats);
  return fighterElement;

}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
