import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createImage } from '../fightersView';

export function showWinnerModal(fighter) {
  // call showModal function
  const title = `${fighter.name} is a winner!`;
  const bodyElement = createElement({ tagName: 'div', className: 'bodyWinner' });
  const imageElement = createImage(fighter);
  bodyElement.append(imageElement);
  const onClose = () => console.log('winner');

  showModal({title, bodyElement, onClose});
}
